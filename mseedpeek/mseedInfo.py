# VERSION 2005.026
# definitions of variables for the various seed blockettes. The
# 0th entry is the title of the blockette

BlkVars = {}
BlkVars[100] = [
    "Sample Rate Blockette 100 (12 bytes)",
    "Blockette Type:",
    "Next Blk Offset:",
    "Actual Sample Rate:",
    "Flags:",
    "Reserved:"
]

BlkVars[200] = [
    "Generic Event Detection Blockette 200 (52 bytes)",
    "Blockette Type:",
    "Next Blk Offset:",
    "Signal Amplitude:",
    "Signal Period:",
    "Background Estimate:",
    "Event Detection Flags:",
    "Reserved:",
    "Signal Onset Time:",
    "Detector Name:",
]

BlkVars[201] = [
    "Murdock Event Detection Blockette 201 (60 bytes)",
    "Blockette Type:",
    "Next Blk Offset:",
    "Signal Amplitude:",
    "Signal Period:",
    "Background Estimate:",
    "Event Detection Flags:",
    "Reserved:",
    "Signal Onset Time:",
    "Signal-to-Noise Ratio Values:",
    "Lookback Value:",
    "Pick Algorithym:",
    "Detector Name:",
]

BlkVars[300] = [
    "Step Calibration Blockette 300 (60 bytes)",
    "Blockette Type:",
    "Next Blk Offset:",
    "Beginning of Calibration Time:",
    "Number of Step Calibrations:",
    "Calibrations Flags:",
    "Step Duration:",
    "Interval Durations:",
    "Calibration Signal Amplitude:",
    "Channel with Calibration Input:",
    "Reserved:",
    "Reference Amplitude:",
    "Coupling:",
    "Rolloff:"
]

BlkVars[310] = [
    "Sine Calibration Blockette 310 (60 bytes)",
    "Blockette Type:",
    "Next Blk Offset:",
    "Beginning of Calibration time:",
    "Reserved:",
    "Calibrations Flags:",
    "Calibration Duration:",
    "Period of Signal:",
    "Amplitude of Signal:",
    "Channel with Calibration input:",
    "Reserved:",
    "Reference Amplitued:",
    "Coupling:",
    "Rolloff:"
]

BlkVars[320] = [
    "Pseudo-random Calibraton Blockette 320 (64 bytes)",
    "Blockette Type:",
    "Next Blk Offset:",
    "Beginning of calibration time:",
    "Reserved:",
    "Calibrations flags:",
    "Calibration duration:",
    "Peak-to-peak amplitude of steps:",
    "Channel with calibration input:",
    "Reserved:",
    "Reference amplitued:",
    "Coupling:",
    "Rolloff:",
    "Noise type:"
]

BlkVars[390] = [
    "Generic Calibraton Blockette 390 (28 bytes)",
    "Blockette Type:",
    "Next Blk Offset:",
    "Beginning of Calibration Time:",
    "Reserved:",
    "Calibrations Flags:",
    "Calibration Duration:",
    "Calibration Signal Amplitude:",
    "Channel with Calibration Input:",
    "Reserved:"
]

BlkVars[395] = [
    "Calibraton Abort Blockette 395 (16 bytes)",
    "Blockette Type:",
    "Next Blk Offset:",
    "End of calibration time:",
    "Reserved:"
]

BlkVars[400] = [
    "Beam Blockette 400 (16 bytes)",
    "Blockette Type:",
    "Next Blk Offset:",
    "Beam Azimuth (degrees):",
    "Beam Slowness (sec/degree):",
    "Beam Configuration:",
    "Reserved:"
]

BlkVars[405] = [
    "Beam Delay Blockette 405 (6 bytes)",
    "Blockette Type:",
    "Next Blk Offset:",
    "Array of Delay Values:"
]

BlkVars[500] = [
    "Timing Blockette 500 (200 bytes)",
    "Blockette Type:",
    "Next Blk Offset:",
    "VCO Correction:",
    "Time of Exception:",
    "microsec:",
    "Reception Quality:",
    "Exception Count:",
    "Exception Type:",
    "Clock Model:",
    "Clock Status:"
]

BlkVars[1000] = [
    "Data Only SEED Blockette 1000 (8 bytes)",
    "Blockette Type:",
    "Next Blk Offset:",
    "Encoding Format:",
    "Word Order:",
    "Data Record Length:",
    "Reserved:"
]

BlkVars[1001] = [
    "Data Extension Blockette 1001 (8 bytes)",
    "Blockette Type:",
    "Next Blk Offset:",
    "Timing Quality:",
    "microsec:",
    "Reserved:",
    "Frame Count:",
]

BlkVars[2000] = [
    "Variable Length Opaque Data Blockette",
    "Blockette Type:",
    "Next Blk Offset:",
    "Total Blockette Length in Bytes:",
    "Offset to Opaque Data:",
    "Record Number:",
    "Data Word Order:",
    "Opaque Data Flags:",
    "Number of Opaque Header Fields:",
    "Opaque Data Header Fields:"
]

# definitions of variables for the various seed blockettes. The
# 0th entry is the title of the blockette
BlkInfoDict = {}

BlkInfoDict["Data Fields"] = """
Field type\tNumber of bits\tField description
UBYTE\t8\t\tUnsigned quantity
BYTE\t8\t\tTwos complement signed quantity
UWORD\t16\t\tUnsigned quantity
WORD\t16\t\tTwos complement signed quantity
ULONG\t32\t\tUnsigned quantity
LONG\t32\t\tTwos complement signed quantity
CHAR*n\tn*8\t\tn characters, each 8 bits and each with a
\t\t\t7-bit ASCII character (high bit always 0)
FLOAT\t32\t\tIEEE Floating point number
"""

BlkInfoDict["BTime"] = """
Field type\tNumber of bits\tField description
UWORD\t16\t\tYear (e.g., 1987)
UWORD\t16\t\tDay of Year (Jan 1 is 1)
UBYTE\t8\t\tHours of day (023)
UBYTE\t8\t\tMinutes of day (059)
UBYTE\t8\t\tSeconds of day (059, 60 for leap seconds)
UBYTE\t8\t\tUnused for data (required for alignment)
UWORD\t16\t\t.0001 seconds (09999)
"""

BlkInfoDict["Fixed Header"] = """
Fixed Section of Data Header (48 bytes)

The data record header starts at the first byte. The next eight bytes follow
the same structure as the control headers. Byte seven contains an ASCII D,
indicating it is a data record. (The eighth byte, or third field, is always
an ASCII space  shown here as a "delta"). The next ten bytes contain the
station, location, and channel identity of the record. The rest of the header
section is binary.

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tSequence number\t\tA\t6\t"######"
2\tData header/quality indicator
\t(D|R|Q)\t\t\tA\t1
3\tReserved byte ("delta")\t\tA\t1
4\tStation identifier code\t\tA\t5\t[UN]
5\tLocation identifier\t\tA\t2\t[UN]
6\tChannel identifier\t\tA\t3\t[UN]
7\tNetwork Code\t\tA\t2
8\tRecord start time\t\tB\t10
9\tNumber of samples\t\tB\t2
10\tSample rate factor\t\tB\t2
11\tSample rate multiplier\t\tB\t2
12\tActivity flags\t\tB\t1
13\tI/O and clock flagst\t\tB\t1
14\tData quality flags\t\tB\t1
15\tNumber of blockettes that
\tfollow\t\t\tB\t1
16\tTime correction\t\tB\t4
17\tBeginning of data\t\tB\t2
18\tFirst blockette\t\tB\t2

Notes for fields: * indicates mandatory information
1 * Data record sequence number (Format "######").
2 * "D" or "R" or "Q"  Data header/quality indicator. Previously, this
      field was only allowed to be "D" and was only used to indicate that
      this is a data header. As of SEED version 2.4 the meaning of this
      field has been extended to also indicate the level of quality control
      that has been applied to the record.
        D  The state of quality control of the data is indeterminate.
        R  Raw Waveform Data with no Quality Control
        Q  Quality Controlled Data, some processes have been applied to the
             data.
3   Space (ASCII 32)  Reserved; do not use this byte.
4 * Station identifier designation (see Appendix G). Left justify and pad with
      spaces.
5 * Location identifier designation. Left justify and pad with spaces.
6 * Channel identifier designation (see Appendix A). Left justify and pad
      with spaces.
7 * A two character alphanumeric identifier that uniquely identifies the
      network operator responsible for the data logger. This identifier is
      assigned by the IRIS Data Management Center in consultation with the
      FDSN working group on the SEED format.
8 * BTIME: Start time of record.
9 * UWORD: Number of samples in record.
10 * WORD: Sample rate factor:
             > 0  Samples/second
             < 0  Seconds/sample
11 * WORD: Sample rate multiplier:
             > 0  Multiplication factor
             < 0  Division factor
12 UBYTE: Activity flags:
             [Bit 0]  Calibration signals present
   *        [Bit 1]  Time correction applied. Set this bit to 1 if the time
                      correction in field 16 has been applied to field 8. Set
                      this bit to 0 if the time correction in field 16 has not
                      been applied to field 8.
            [Bit 2]  Beginning of an event, station trigger
            [Bit 3]  End of the event, station detriggers
            [Bit 4]  A positive leap second happened during this record (A 61
                      second minute).
            [Bit 5]  A negative leap second happened during this record (A 59
                      second minute). A negative leap second clock correction
                      has not yet been used, but the U.S. National Bureau of
                      Standards has said that it might be necessary someday.
            [Bit 6]  Event in progress
13 UBYTE: I/O flags and clock flags:
            [Bit 0]  Station volume parity error possibly present
            [Bit 1]  Long record read (possibly no problem)
            [Bit 2]  Short record read (record padded)
            [Bit 3]  Start of time series
            [Bit 4]  End of time series
            [Bit 5]  Clock locked
14 UBYTE: Data quality flags
            [Bit 0]  Amplifier saturation detected (station dependent)
            [Bit 1]  Digitizer clipping detected
            [Bit 2]  Spikes detected
            [Bit 3]  Glitches detected
            [Bit 4]  Missing/padded data present
            [Bit 5]  Telemetry synchronization error
            [Bit 6]  A digital filter may be charging
            [Bit 7]  Time tag is questionable
15 * UBYTE: Total number of blockettes that follow.
16 * LONG: Time correction. This field contains a value that may modify the
             field 8 record start time. Depending on the setting of bit 1 in
             field 12, the record start time may have already been adjusted.
             The units are in 0.0001 seconds.
17 * UWORD: Offset in bytes to the beginning of data. The first byte of the
              data records is byte 0.
18 * UWORD: Offset in bytes to the first data blockette in this data record.
              Enter 0 if there are no data blockettes. The first byte in the
              data record is byte offset 0.

NOTE: All unused bits in the flag bytes are reserved and must be set to zero.
The last word defines the length of the fixed header. The next-to-last word
fixes the length reserved for the entire header.

If glitches (missing samples) are detected, set bit 3 of the data quality
flags, and code missing data values as the largest possible value. Do this for
any data format, even if you are using theSteim Compression algorithm.

Here is an algorithm and some sample rate combinations that describe how the
sample rate factors and multipliers work:

If Sample rate factor > 0 and Sample rate Multiplier > 0,
    Then nominal Sample rate = Sample rate factor X Sample rate multiplier
If Sample rate factor > 0 and Sample rate Multiplier < 0,
    Then nominal Sample rate = -1 X Sample rate factor / Sample rate multiplier
If Sample rate factor < 0 and Sample rate Multiplier > 0,
    Then nominal Sample rate = -1 X Sample rate multiplier / Sample rate factor
If Sample rate factor < 0 and Sample rate Multiplier < 0,
    Then nominal Sample rate = 1/ (Sample rate factor X Sample rate multiplier)

Sample rate\t\tSample rate factor\tSample rate multiplier
330 SPS\t\t33\t\t10
330\t\t1
330.6 SPS\t\t3306\t\t-10
1 SP Min\t\t-60\t\t1
0.1 SPS\t\t1\t\t-10
-10\t\t1
-1\t\t-10
"""

BlkInfoDict[100] = """
Sample Rate Blockette 100 (12 bytes)

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tBlockette type 100\t\tB\t2
2\tNext blockettes byte number\tB\t2
3\tActual Sample Rate\t\tB\t4
4\tFlags (to be defined)\t\tB\t1
5\tReserved byte\t\tB\t3

Notes for fields:
1 UWORD:\tBlockette type [100]: sample rate.
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
\tfrom the beginning of the logical record  including the fixed
\tsection of the data header; use 0 if no more blockettes will follow.)
3 FLOAT:\tActual sample rate of this data block.
4 BYTE:\tFlags (to be defined)
5 UBYTE:\tReserved; do not use.
"""
BlkInfoDict[200] = """
Generic Event Detection Blockette 200 (52 bytes)

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tBlockette type 200\t\tB\t2
2\tNext blockettes byte number\tB\t2
3\tSignal amplitude\t\tB\t4
4\tSignal period\t\tB\t4
5\tBackground estimate\t\tB\t4
6\tEvent detection flags\t\tB\t1
7\tReserved byte\t\tB\t1
8\tSignal onset time\t\tB\t10
9\tDetector Name\t\tA\t24

Notes for fields:
1 UWORD:\tBlockette type [200]: event detection information.
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
\tfrom the beginning of the logical record  including the fixed
\tsection of the data header; use 0 if no more blockettes will follow.)
3 FLOAT:\tAmplitude of signal (for units, see event detection flags,
\tbelow; 0 if unknown).
4 FLOAT:\tPeriod of signal, in seconds (0 if unknown).
5 FLOAT:\tBackground estimate (for units, see event detection flags,
\tbelow; 0 if unknown).
6 UBYTE:\tEvent detection flags:
\t[Bit 0]  If set: dilatation wave; if unset: compression
\t[Bit 1]  If set: units above are after deconvolution
\t            (see Channel Identifier Blockette [52], field 8);
\t            if unset: digital counts
\t[Bit 2]  When set, bit 0 is undetermined
\t[Other bits reserved and must be zero.]
7 UBYTE:\tReserved; do not use.
8 BTIME:\tTime of the onset of the signal.
9 CHAR*24: The name of the event detector.
"""
BlkInfoDict[201] = """
Murdock Event Detection Blockette 201 (60 bytes)

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tBlockette type 201\t\tB\t2
2\tNext blockettes byte number\tB\t2
3\tSignal amplitude\t\tB\t4
4\tSignal period\t\tB\t4
5\tBackground estimate\t\tB\t4
6\tEvent detection flags\t\tB\t1
7\tReserved byte\t\tB\t1
8\tSignal onset time\t\tB\t10
9\tSignal-to-noise ratio values\tB\t6
10\tLookback value\t\tB\t1
11\tPick algorithm\t\tB\t1
12\tDetector name\t\tA\t24

Notes for fields:
1 UWORD:\tBlockette type [201]: event detection information.
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
\tfrom the beginning of the logical record  including the fixed
\tsection of the data header; use 0 if no more blockettes will follow.)
3 FLOAT:\tAmplitude of signal (in counts).
4 FLOAT:\tPeriod of signal (in seconds).
5 FLOAT:\tBackground estimate (in counts).
6 UBYTE:\tEvent detection flags:
\t[Bit 0]  If set: dilatation wave; if unset: compression
\t[Other bits reserved and must be zero.]
7 UBYTE:\tReserved; do not use.
8 BTIME:\tOnset time of the signal.
9 UBYTE*6:\tSignal-to-noise ratio values.
10 UBYTE:\tLookback value (0,1,2).
11 UBYTE:\tPick algorithm (0,1).
12 CHAR*24: The name of the event detector.

NOTE: See Murdock (1983) and Murdock (1987) for more information on this
type of eventdetector, and on what the fields listed above should contain.
"""

BlkInfoDict[300] = """
Step Calibration Blockette 300 (60 bytes)

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tBlockette type 300\t\tB\t2
2\tNext blockettes byte number\tB\t2
3\tBeginning of calibration time\tB\t10
4\tNumber of step calibrations\tB\t1
5\tCalibration flags\t\tB\t1
6\tStep duration\t\tB\t4
7\tInterval duration\t\tB\t4
8\tCalibration signal amplitude\tB\t4
9\tChannel w/ calibration input\tA\t3
10\tReserved byte\t\tB\t1
11\tReference amplitude\t\tB\t4
12\tCoupling\t\t\tA\t12
13\tRolloff\t\t\tA\t12

Notes for fields:
1 UWORD:\tBlockette type[300]: step calibration.
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
\tfrom the beginning of the logical record  including the fixed
\tsection of the data header; use 0 if no more blockettes will follow.)
3 BTIME:\tBeginning time of calibration.
4 UBYTE:\tNumber of step calibrations in sequence.
5 UBYTE:\tCalibration flags:
\t[Bit 0]  If set: first pulse is positive
\t[Bit 1]  If set: calibrations alternate sign
\t[Bit 2]  If set: calibration was automatic; if unset: manual
\t[Bit 3]  If set: calibration continued from previous record(s)
\t[Other bits reserved and must be zero.]
6 ULONG:\tNumber of .0001 second ticks for the duration of the step.
7 ULONG:\tNumber of .0001 second ticks for the interval between times the
\tcalibration step is on.
8 FLOAT:\tAmplitude of calibration signal in units (see Channel Identifier
\tBlockette [52), field 9).
9 CHAR*3:\tChannel containing calibration input (blank means none).
\tSEED assumes that the calibration output is on the current
\tchannel, identified in the fixed header.
10 UBYTE:\tReserved; do not use.
11 ULONG:\tReference amplitude. This is a user defined value that indicates
\teither the voltage or amperage of the calibration signal when the
\tcalibrator is set to 0dB. If this value is zero, then no units are
\tspecified, and the amplitude (Note 4) will be reported in "binary
\tdecibels" from to -96.
12 CHAR*12: Coupling of calibration signal, such as "Resistive " or
\t"Capacitive".
13 CHAR*12: Rolloff characteristics for any filters used on the calibrator,
\tsuch as "3dB@10Hz".
"""

BlkInfoDict[310] = """
Sine Calibration Blockette 310 (60 bytes)

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tBlockette type 310\t\tB\t2
2\tNext blockettes byte number\tB\t2
3\tBeginning of calibration time\tB\t10
4\tReserved byte\t\tB\t1
5\tCalibration flags\t\tB\t1
6\tCalibration duration\t\tB\t4
7\tPeriod of signal (seconds)\tB\t4
8\tAmplitude of signal\t\tB\t4
9\tChannel w/ calibration input\tA\t3
10\tReserved byte\t\tB\t1
11\tReference amplitude\t\tB\t4
12\tCoupling\t\t\tA\t12
13\tRolloff\t\t\tA\t12

Notes for fields:
1 UWORD:\tBlockette type [310]: sine calibration.
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
\tfrom the beginning of the logical record  including the fixed
\tsection of the data header; use 0 if no more blockettes will follow.)
3 BTIME:\tBeginning time of calibration.
4 UBYTE:\tReserved; do not use.
5 UBYTE:\tCalibration flags:
\t[Bit 2]  If set: calibration was automatic; otherwise: manual
\t[Bit 3]  If set: calibration continued from previous record(s)
\t[Bit 4]  If set: peak-to-peak amplitude
\t[Bit 5]  If set: zero-to-peak amplitude
\t[Bit 6]  If set: RMS amplitude
\t[Other bits reserved and must be zero.]
6 ULONG:\tNumber of .0001 second ticks for the duration of calibration.
7 FLOAT:\tPeriod of signal in seconds.
8 FLOAT:\tAmplitude of signal in units (see Channel Identifier Blockette [52),
\tfield 9).
9 CHAR*3:\tChannel containing calibration input (blank means none).
10 UBYTE:\tReserved; do not use.
11 ULONG:\tReference amplitude. This is a user defined value that indicates
\teither the voltage or amperage of the calibration signal when the
\tcalibrator is set to 0dB. If this value is zero, then no units are
\tspecified, and the amplitude (Note 4) will be reported in "binary
\tdecibels" from 0 to -96.
12 CHAR*12: Coupling of calibration signal such as "Resistive or "Capacitive".
13 CHAR*12: Rolloff characteristics for any filters used on the calibration,
\tsuch as "3dB@10Hz".

NOTE: Only one of flag bits 4, 5, and 6 can be set at one time, but one of
them must be set.
"""

BlkInfoDict[320] = """
Pseudo-random Calibraton Blockette 320 (64 bytes)

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tBlockette type 320\t\tB\t2
2\tNext blockettes byte number\tB\t2
3\tBeginning of calibration time\tB\t10
4\tReserved byte\t\tB\t1
5\tCalibration flags\t\tB\t1
6\tCalibration duration\t\tB\t4
7\tPeak-to-peak amplitude of
\tsteps\t\t\tB\t4
8\tChannel w/ calibration input\tA\t3
9\tReserved byte\t\tB\t1
10\tReference amplitude\t\tB\t4
11\tCoupling\t\t\tA\t12
12\tRolloff\t\t\tA\t12
13\tNoise type\t\t\tA\t8

Notes for fields:
1 UWORD:\tBlockette type [320]: pseudo-random binary sequence.
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
        \tfrom the beginning of the logical record  including the fixed
        \tsection of the data header; use 0 if no more blockettes will follow.)
3 BTIME:\tBeginning time of calibration.
4 UBYTE:\tReserved; do not use.
5 UBYTE:\tCalibration flags:
        \t[Bit 2]  If set: calibration was automatic; otherwise: manual
        \t[Bit 3]  If set: calibration continued from previous record(s)
        \t[Bit 4]  If set: random amplitudes
        \t(must have a calibration in channel)
        \t[Other bits reserved and must be zero.]
6 ULONG:\tNumber of .0001 second ticks for the duration of calibration.
7 FLOAT:\tPeak-to-peak amplitude of steps in units (see Channel Identifier
        \tBlockette [52], field 9).
8 CHAR*3:\tChannel containing calibration input (blank if none).
9 UBYTE:\tReserved; do not use.
10 ULONG:\tReference amplitude. This is a user defined value that indicates
        \teither the voltage or amperage of the calibration signal when the
        \tcalibrator is set to 0dB. If this value is zero, then no units are
        \tspecified, and the amplitude (Note 4) will be reported in "binary
        \tdecibels" from 0 to -96.
11 CHAR*12: Coupling of calibration signal such as "Resistive or "Capacitive".
12 CHAR*12: Rolloff characteristics for any filters used on the calibration,
        \tsuch as "3dB@10Hz".
13 CHAR*8:\tNoise characteristics, such as "White" or "Red".

NOTE: When you set calibration flag bit 4, the amplitude value contains the
maximum peakto-peak amplitude.
"""

BlkInfoDict[390] = """
Generic Calibraton Blockette 390 (28 bytes)

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tBlockette type 390\t\tB\t2
2\tNext blockettes byte number\tB\t2
3\tBeginning of calibration time\tB\t10
4\tReserved byte\t\tB\t1
5\tCalibration flags\t\tB\t1
6\tCalibration duration\t\tB\t4
7\tCalibration signal amplitude\tB\t4
8\tChannel w/ calibration input\tA\t3
9\tReserved byte\t\tB\t1

Notes for fields:
1 UWORD:\tBlockette type [390]: generic calibration.
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
\tfrom the beginning of the logical record  including the fixed
\tsection of the data header; use 0 if no more blockettes will follow.)
3 BTIME:\tBeginning time of calibration.
4 UBYTE:\tReserved; do not use.
5 UBYTE:\tCalibration flags:
\t[Bit 2]  If set: calibration was automatic; otherwise: manual
\t[Bit 3]  If set: calibration continued from previous record(s)
\t[Other bits reserved and must be zero.]
6 ULONG:\tNumber of .0001 second ticks for the duration of calibration.
7 FLOAT:\tAmplitude of calibration in units, if known (see Channel Identifier
\tBlockette [52], field 9).
8 CHAR*3:\tChannel containing calibration input (must be specified).
9 UBYTE:\tReserved; do not use.
"""

BlkInfoDict[395] = """
Calibraton Abort Blockette 395 (16 bytes)

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tBlockette type 395\t\tB\t2
2\tNext blockettes byte number\tB\t2
3\tEnd of calibration time\t\tB\t10
4\tReserved bytes\t\tB\t2

Notes for fields:
1 UWORD:\tBlockette type [395]: calibration abort.
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
\tfrom the beginning of the logical record  including the fixed
\tsection of the data header; use 0 if no more blockettes will follow.)
3 BTIME:\tTime calibration ends.
4 UWORD:\tReserved; do not use.
"""

BlkInfoDict[400] = """
Beam Blockette 400 (16 bytes)

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tBlockette type 400\t\tB\t2
2\tNext blockettes byte number\tB\t2
3\tBeam azimuth (degrees)\tB\t4
4\tBeam slowness (sec/degree)\tB\t4
5\tBeam configuration\t\tB\t2
6\tReserved bytes\t\tB\t2

This blockette is used to specify how the beam indicated by the corresponding
Beam Configuration Blockette [35] was formed for this data record. For beams
formed by non-plane waves, the Beam Delay Blockette [405] should be used to
determine the beam delay for each component referred to in the Beam
Configuration Blockette [35].

Notes for fields:
1 UWORD:\tBlockette type [400]: beam forming.
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
\tfrom the beginning of the logical record  including the fixed
\tsection of the data header; use 0 if no more blockettes will follow.)
3 FLOAT:\tAzimuth of beam (degrees clockwise from north).
4 FLOAT:\tBeam slowness (sec/degree).
5 UWORD:\tBeam configuration (see field 3 of the Beam Configuration Blockette
\t[35] abbreviation dictionary).
\tNOTE: This field is a binary equivalent of the ASCII formatted
\tdictionary key entry number in the Beam Configuration Blockette [35].
\tThis is the only place in SEED Version 2.1 where this ASCII-to-binary
\tconversion needs to be made.
6 UWORD:\tReserved; do not use.
"""

BlkInfoDict[405] = """
Beam Delay Blockette 405 (6 bytes)

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tBlockette type 405\t\tB\t2
2\tNext blockettes byte number\tB\t2
3\tArray of delay values\t\tB\t2

Use this blockette to define beams that do not travel as plane waves at
constant velocities across arrays. This blockette, if used, will always
follow a Beam Blockette [400]. The Beam Delay Blockette [405] describes
the delay for each input component in the samples. SEED reading programs
must find a corresponding entry in the Beam Delay Blockette [405] for each
component in the Beam Configuration Blockette [35] of the abbreviation
dictionary control headers, indexed by the Beam Blockette [400].

Notes for fields:
1 UWORD:\tBlockette type [405]: beam delay.
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
\tfrom the beginning of the logical record  including the fixed
\tsection of the data header; use 0 if no more blockettes will follow.)
3 UWORD:\tArray of delay values (one for each entry of the Beam Configuration
\tBlockette [35]. The array values are in .0001 second ticks.
"""

BlkInfoDict[500] = """
Timing Blockette 500 (200 bytes)

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tBlockette type 500\t\tB\t2
2\tNext blockettes byte number\tB\t2
3\tVCO correction\t\tB\t4
4\tTime of exception\t\tB\t10
5\tmicrosec\t\t\tB\t1
6\tReception Quality\t\tB\t1
7\tException count\t\tB\t4
8\tException type\t\tA\t16
9\tClock model\t\tA\t32
10\tClock status\t\t\tA\t128

Notes for fields:
1 UWORD:\tBlockette type [500]: Timing blockette.
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
\tfrom the beginning of the logical record  including the fixed
\tsection of the data header; use 0 if no more blockettes will follow.)
3 FLOAT:\tVCO correction is a floating point percentage from 0.0 to 100.0% of
\tVCO control value, where 0.0 is slowest , and 100.0% is fastest.
4 BTIME:\tTime of exception, same format as record start time.
5 UBYTE:\tmicrosec has the clock time down to the microsecond. The SEED format
\thandles down to 100microsecs. This field is an offset from that value.
\tThe recommended value is from -50 to +49microsecs. At the users
\toption, this value may be from 0 to +99microsecs.
6 UBYTE:\tReception quality is a number from 0 to 100% of maximum clock
\taccuracy based only on information from the clock.
7 ULONG:\tException count is an integer count, with its meaning based on the
\ttype of exception, such as 15 missing timemarks.
8 CHAR*16:\tException type describes the type of clock exception, such as
\t"Missing" or "Unexpected".
9 CHAR*32:\tClock model is an optional description of the clock, such as
\t"Quanterra GPS1/QTS".
10 CHAR*128: Clock status is an optional description of clock specific
\tparameters, such as the station for an Omega clock, or satellite
\tsignal to noise ratios for GPS clocks.
"""

BlkInfoDict[1000] = """
Data Only SEED Blockette 1000 (8 bytes)

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tBlockette type 1000\t\tB\t2
2\tNext blockettes byte number\tB\t2
3\tEncoding Format\t\tB\t1
4\tWord order\t\t\tB\t1
5\tData Record Length\t\tB\t1
6\tReserved\t\t\tB\t1

Notes for fields:
1 UWORD:\tBlockette type [1000]: Data Only SEED
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
\tfrom the beginning of the logical record  including the fixed
\tsection of the data header; use 0 if no more blockettes will follow.)
3. BYTE:\tA code indicating the encoding format. This number is assigned by
\tthe FDSN Data Exchange Working Group. To request that a new format
\tbe included contact the FDSN through the FDSN Archive at the IRIS
\tData Management Center. To be supported in Data Only SEED, the dat
\tformat must be expressible in SEED DDL. A list of valid codes at the
\ttime of publication follows.

\tCODES 0-9 GENERAL
\t0\tASCII text, byte order as specified in field 4
\t1\t16 bit integers
\t2\t24 bit integers
\t3\t32 bit integers
\t4\tIEEE floating point
\t5\tIEEE double precision floating point

\tCODES 10 - 29 FDSN Networks
\t10\tSTEIM (1) Compression
\t11\tSTEIM (2) Compression
\t12\tGEOSCOPE Multiplexed Format 24 bit integer
\t13\tGEOSCOPE Multiplexed Format 16 bit gain ranged, 3 bit exponent
\t14\tGEOSCOPE Multiplexed Format 16 bit gain ranged, 4 bit exponent
\t15\tUS National Network compression
\t16\tCDSN 16 bit gain ranged
\t17\tGraefenberg 16 bit gain ranged
\t18\tIPG - Strasbourg 16 bit gain ranged
\t19\tSTEIM (3) Compression

\tCODES 30 - 39 OLDER NETWORKS
\t30\tSRO Format
\t31\tHGLP Format
\t32\tDWWSSN Gain Ranged Format
\t33\tRSTN 16 bit gain ranged

4. The byte swapping order for 16 bit and 32 bit words. A 0 indicates VAX or
    8086 order and a 1 indicates 68000 or SPARC word order. See fields 11 and
    12 of blockette 50.
5. The exponent (as a power of two) of the record length for these data. The
    data record can be as small as 256 bytes and, in Data Only SEED format as
    large as 2 raised to the 256 power.
"""

BlkInfoDict[1001] = """
Data Extension Blockette 1001 (8 bytes)

Note\tField Name\t\t\tType\tLength\tMask or Flags
1\tBlockette type 1001\t\tB\t2
2\tNext blockettes byte number\tB\t2
3\tTiming quality\t\tB\t1
4\tmicrosec \t\t\tB\t1
5\tReserved\t\t\tB\t1
6\tFrame count\t\tB\t1

Notes for fields:
1 UWORD:\tBlockette type [1001]: Data extension blockette
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
\tfrom the beginning of the logical record  including the fixed
\tsection of the data header; use 0 if no more blockettes will follow.)
3. UBYTE:\tTiming quality is a vendor specific value from 0 to 100% of maxium
\taccuracy, taking into account both clock quality and data flags.
4. UBYTE:\tmicrosec has the data start time down to the microsecond. The SEED
\tformat handles down to 100microsecs. This field is an offset from
\tthat value. The recommended value is from -50 to +49microsecs. At
\tthe users option, this value may be from 0 to +99microsecs.
5. Reserved byte.
6. UBYTE:\tFrame count is the number of 64 byte compressed data frames in the
\t4K record (maximum of 63). Note that the user may specify fewer than
\tthe maxium allowable frames in a 4K record to reduce latency
"""

BlkInfoDict[2000] = """
Variable Length Opaque Data Blockette

Note\tField Name\t\t\tType\tLength\tOffset
1\tBlockette type 2000\t\tB\t2\t0
2\tNext blockettes byte number\tB\t2\t2
3\tTotal blockette length in
\tbytes\t\t\tB\t2\t4
4\tOffset to Opaque Data\t\tB\t2\t6
5\tRecord number\t\tB\t4\t8
6\tData Word order\t\tB\t1\t12
7\tOpaque Data flags\t\tB\t1\t13
8\tNumber Opaque Header fields\tB\t1\t14
9\tOpaque Data Header fields\tV\tV\t15
\ta Record type
\tb Vendor type
\tc Model type
\td Software
\te Firmware
10\tOpaque Data\t\tOpaque

More than one blockette 2000 may be stored in a SEED data record if the
SEED data record timetag is not required for precise timing of the data
in the opaque blockette.

Under normal usage, there would be no data in the data portion of the SEED
data record. However, it is possible that the blockette 2000 could be used
to provide additional information
for a normal timeseries data channel.

Notes for fields:
1 UWORD:\tBlockette type [2000]: Opaque Data blockette
2 UWORD:\tByte number of next blockette. (Calculate this as the byte offset
\tfrom the beginning of the logical record  including the fixed
\tsection of the data header; use 0 if no more blockettes will follow.)
3 UWORD:\tBlockette length. The total number of bytes in this blockette,
\tincluding the 6 bytes of header. The only restriction is that the
\tblockette must fit within a single SEED data record for the channel.
\tOtherwise, the blockette must be partitioned into multiple blockettes.
4 UWORD:\tOffset to Opaque Data. Byte offset from beginning of blockette to
\tOpaque Data.
5 ULONG:\tRecord Number. The record number may be used for sequence
\tidentification of stream, record, or file oriented data. If a record
\tis partitioned into multiple opaque blockettes, each blockette
\tcontaining a portion of the record should contain the identical record
\tnumber. It is strongly recommended that the record number be used to
\taid in the detection of missing data and in merging data from
\tdifferent telemetry streams. Use 0 if data is not record oriented, or
\tif record number is not required.
6 UBYTE:\tWord order of binary opaque data. See field 4 of blockette 1000, and
\tfields 11 and 12 of blockette 50.
\t     0 = little endian (VAX or 80x86 byte order).
\t     1 = big endian (68000 or SPARC byte order).
7 UBYTE:\tOpaque Data flags.
\t[bit 0] Opaque blockette orientation.
\t     0 = record oriented.
\t     1 = stream oriented.
\t[bit 1] Packaging bit.
\t     0 = Blockette 2000s from multiple SEED data records with
\t          different timetags may be packaged into a single SEED data
\t          record. The exact original timetag in each SEED Fixed Data
\t          Header is not required for each blockette 2000.
\t     1 = Blockette 2000s from multiple SEED data records with
\t          differing timetags may NOT be repackaged into a single SEED
\t          data record. Set this bit if the timetag in the SEED Fixed
\t          Data Header is required to properly interpret the opaque
\t          data.
\t[bits 2-3] Opaque blockette fragmentation flags.
\t     00 = opaque record identified by record number is completely
\t           contained in this opaque blockette.
\t     01 = first opaque blockette for record spanning multiple
\t           blockettes.
\t     11 = continuation blockette 2...N-1 of record spanning N
\t           blockettes.
\t     10 = final blockette for record spanning N blockettes.
\t[bits 4-5] File blockette information.
\t     00 = not file oriented.
\t     01 = first blockette of file.
\t     10 = continuation of file.
\t     11 = last blockette of file.
8 UBYTE:\tNumber of Opaque Header fields. Each opaque header field is a
\tvariable
\tlength ascii string, terminated by the character ~.
9 VAR:\tOpaque Data Header string, which contains the ascii variable length
\tfields. Each field is terminated by a "~". The definition of the fields
\tmay be defined by the originator and receiver, but the following are
\trecommended.
\tAny of the fields may be empty.
\t     a    Record Type - name of the type of record (eg "GPS", "GPS MBEN").
\t     b    Vendor Type - name of equipment vendor (eg "ASHTECH").
\t     c    Model type - model type of equipment (eg "Z12").
\t     d   Software Version - software version number (eg "").
\t     e   Firmware Version - firmware version number (eg "1G0C").
10 OPAQUE: Opaque Data - bytes of opaque data. Total length of opaque data in
\tbytes is blockette_length - 15 - length (opaque_data_header_string)
"""
