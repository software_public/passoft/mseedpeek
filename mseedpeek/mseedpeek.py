"""
!/usr/bin/env python


# gui for viewing mseed headers
# author: bcb
#
##########################
# 2004.141
# Modification
# Author: bcb
# beautified help page
#
# NOTE: next change to have drop down for blockette notebook
# and actually describe each field for given blockette

##########################
# 2004.301
# Modification
# Author: bcb
#
# utilize LibTrace calcrate function rather than local

##########################
# 2005.027
# Modification
# Author: bcb
#
# added try/except for directory & file open in self.FindTrace to
#     reflect changes in LibTrace.py
# incorporated mseedInfo.py - file containing mseed header information
# added dropdown menus for blockettes and labeled display
# added Blockette Information window
# re-disorganized routines and added a few additional code comments
# optimized FindTrace

##########################
# 2005.040
# Modification
# Author: bcb
#
# added infobox indicating endianess
# correctly handles mseed files that are corrupt

##########################
# 2005.132
# Modification
# Author: bcb
#
# removed routine GetBlockette and replaced with 2 lines of code

##########################
# 2005.143
# Modification
# Author: bcb
#
# added main window button "Flush Dictionaries". Allows user view changes to
# mseed
# files without rebuilding db

##########################
# 2005.281
# Modification
# Author: bcb
#
# added a station selection box to window on station name
# added a -f option to allow user to give filenames on commandline
# using getopt for cmdline parsing
##########################
# 2005.307
# Modification
# Author: bcb
#
# added feature if only one trace on commandline input then fill fields on
# startup
##########################
# 2005.335
# bug fix
# Author: bcb
#
# blockettes 100, 201 and 400 were not being handled correctly in ReadHdrs
##########################
# 2007.040
# modification
# Author: bcb
#
# added capability to select viewed sta:chan:loc:net:samp in Unique view
#
# new methods: buildUniqueSelectBox, SelectKeys
################################################################
#
# modification
# version: 2008.180
# author: Bruce Beaudoin
#
# modified to take advantage of LibTrace optimization
################################################################
#
# modification
# version: 2018.129
# author: Derick Hess
#
# Python 3 compatibility
# setup.py
################################################################
#
# modification
# version: 2020.204
# author: Maeva Pourpoint
#
# Updates to work under Python 3.
# Unit tests to ensure basic functionality.
# Code cleanup to conform to the PEP8 style guide.
# Directory cleanup (remove unused files introduced by Cookiecutter).
# Packaged with conda.
################################################################
#
# modification
# version: 2022.1.0.0
# author: Maeva Pourpoint
#
# Update versioning scheme
################################################################
#
# modification
# version: 2022.2.0.0
# author: Destiny Kuehn
#
# GUI updated to PySide2
# Pmw dependency dropped
# Version number updated
# Run build trace to thread
################################################################
#
# modification
# version: 2024.4.0.0
# author: Omid Hosseini
#
# GUI updated to PySide6
# Version number updated
"""

import sys
import os
import itertools

from getopt import getopt
from glob import glob
from operator import mod
from PySide6.QtWidgets import (QApplication, QWidget, QTabWidget,
                               QLineEdit, QVBoxLayout, QHBoxLayout,
                               QTextEdit, QGroupBox, QLabel,
                               QComboBox, QPushButton, QSizePolicy,
                               QFormLayout, QRadioButton, QSlider,
                               QSpacerItem, QStackedWidget, QFileDialog,
                               QMessageBox, QProgressDialog, QTreeWidget,
                               QTreeWidgetItem)
from PySide6.QtCore import (Qt, QObject, Signal, QThread, QTimer, QEventLoop)
from PySide6.QtGui import QColor, QBrush

from mseedpeek.libtrace import *
from mseedpeek.mseedInfo import *

VERSION = "2024.4.0.1"


def main():
    """
    Get commandline args and launch mseedpeek
    """
    # return version number if -# command line option
    file_list = []
    try:
        opts, pargs = getopt(sys.argv[1:], 'f#')
        for flag, arg in opts:
            if flag == "-f":
                InFiles = pargs
                for fl in InFiles:
                    for f in glob(fl):
                        file_list.append(f)
            if flag == "-#":
                print(VERSION)
                sys.exit(0)
    except Exception:
        print("\nInvalid command line usage\n")
        print("Usage:\nmseedpeek\nmseedpeek -#\nmseedpeek -f file(s)\n")
        sys.exit(1)
    print("\n", os.path.basename(sys.argv[0]), VERSION)

    app = QApplication(sys.argv)
    # mc = main class
    if file_list:
        mc = MainWindow("mseedpeek %s" % VERSION, file_list)
    else:
        mc = MainWindow("mseedpeek %s" % VERSION, file_list=None)
    mc.show()
    sys.exit(app.exec_())


class MainWindow(QWidget):
    """
    Main class for mseedpeek
    """
    def __init__(self, title, file_list):
        """
        Init everything for startup
        """
        super().__init__()

        # window settings
        self.resize(self.minimumSize())
        self.setWindowTitle(title)

        # init vars
        self.data_init()

        # set up main window
        self.create_main_window()
        self.build_blockettes()
        self.build_help()
        self.hide_show("hide")

        # if files entered on commandline call build trace
        if file_list:
            self.dd_text.setText(':'.join(str(file) for file in file_list))
            self.build_trace_list(self.dd_text.text())

    def data_init(self):
        """
        Init starting vars
        """
        self.verb_var = 0
        self.blk_size = 0
        self.num_blocks = 0
        self.dir_list = []
        self.stat_sel_list = []
        self.old_mseed_file = ""

        self.standard_vars = [
            "Station Name:",
            "Location Code:",
            "Channel:",
            "Net Code:",
            "Sps (nominal):",
        ]

        self.v_vars = [
            "Year:",
            "Day:",
            "Hour:",
            "Min:",
            "Sec:",
            "0.0001s:"
        ]

        self.first_vv_vars = [
            "Sequence Number:",
            "Data Hdr/Qaul:",
            "Reserved:"
        ]

        self.vv_vars = [
            "Number Samples:",
            "Sample Factor:",
            "Sample Multiplier:",
            "Activity Flags:",
            "I/O & Clk Flags:",
            "Data Qaul Flags:",
            "Number Blockettes:",
            "Time Corr:",
            "Offet Data:",
            "Offset Blockette:"
        ]

    def create_main_window(self):
        """
        Build tabs and info bar for root window
        """
        # tab widget
        self.tabwidget = QTabWidget()
        self.trace_headers_tab = QWidget()
        self.blockettes_tab = QWidget()
        self.help_tab = QWidget()
        self.tabwidget.addTab(self.trace_headers_tab, "Trace Headers")
        self.tabwidget.addTab(self.blockettes_tab, "Blockettes")
        self.tabwidget.addTab(self.help_tab, "Help")

        # info bar widget
        self.infobar = QLineEdit()
        self.infobar.setStyleSheet("background-color:yellow")
        self.infobar.setReadOnly(True)
        self.infobar.setAlignment(Qt.AlignCenter)

        # slider
        self.slider_box = SliderBox()
        self.tabwidget.currentChanged.connect(
            self.set_slider_visibility)
        self.slider_box.slider.valueChanged.connect(
            lambda: self.update_slider("slider"))
        self.slider_box.jump_menu.currentTextChanged.connect(
            lambda: self.update_slider("menu"))

        # main window layout
        self.window_layout = QVBoxLayout()
        self.setLayout(self.window_layout)

        # add tab widget and info bar to main window layout
        self.window_layout.addWidget(self.tabwidget)
        self.window_layout.addWidget(self.slider_box)
        self.build_trace_headers()  # build widgets for Trace Headers tab
        self.window_layout.addWidget(self.infobar)
        self.window_layout.setContentsMargins(0, 0, 0, 0)  # set spacing

    def set_slider_visibility(self):
        # hide slider if user in help tab
        if self.tabwidget.currentIndex() == 2:
            self.slider_box.hide()
        else:
            if self.tabwidget.currentIndex() == 0 and \
               self.verb_var == 3:
                self.slider_box.hide()
            else:
                self.slider_box.show()

    def build_help(self):
        """
        Build Help tab
        """
        # init tab layout
        layout = QVBoxLayout(self.help_tab)

        # Help textbox
        Blue = QColor(0, 0, 153)
        Green = QColor(0, 100, 0)
        Red = QColor(136, 8, 8)
        Black = QColor(0, 0, 0)

        help_text = QTextEdit()
        layout.addWidget(help_text)
        help_text.setVerticalScrollBarPolicy(
            Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        help_text.setAcceptRichText(False)

        # Name
        help_text.setTextColor(Blue)
        help_text.insertPlainText("NAME")
        help_text.setTextColor(Black)
        help_text.insertPlainText("""
        mseedpeek - GUI for displaying mseed file headers.\n\n""")

        # Version
        help_text.setTextColor(Blue)
        help_text.insertPlainText("VERSION")
        help_text.setTextColor(Black)
        text = "\n" + SPACE + str(VERSION) + "\n\n"
        help_text.insertPlainText("""
        %s\n\n""" % VERSION)

        # Synopsis
        help_text.setTextColor(Blue)
        help_text.insertPlainText("SYNOPSIS")
        help_text.setTextColor(Black)
        help_text.insertPlainText("""
        mseedpeek
        mseedpeek -#
        mseedpeek -f file_names\n\n""")

        # Options
        help_text.setTextColor(Blue)
        help_text.insertPlainText("OPTIONS")
        help_text.setTextColor(Black)
        help_text.insertPlainText("""
        -# returns version number
        -f file_name(s) - accepts command line input for files to inspect (wildcards accepted)\n\n""")  # noqa: E501

        # Description
        help_text.setTextColor(Blue)
        help_text.insertPlainText("DESCRIPTION")
        help_text.setTextColor(Black)
        help_text.insertPlainText("""
        mseedpeek has three notebooks: """)
        help_text.setTextColor(Green)
        help_text.insertPlainText("[Trace Headers][Blockettes]")
        help_text.setTextColor(Black)
        help_text.insertPlainText(", and ")
        help_text.setTextColor(Green)
        help_text.insertPlainText("[Help]")
        help_text.setTextColor(Black)
        help_text.insertPlainText(".")
        help_text.insertPlainText("""
        The """)
        help_text.setTextColor(Green)
        help_text.insertPlainText("[Trace Headers]")
        help_text.setTextColor(Black)
        help_text.insertPlainText(
            """ notebook displays various the fixed header in""" +
            """four levels of verbosity.
        The """)
        help_text.setTextColor(Green)
        help_text.insertPlainText("[Blockettes]")
        help_text.setTextColor(Black)
        help_text.insertPlainText(""" notebook displays blockette information.\n""")  # noqa: E501)
        help_text.setTextColor(Green)
        help_text.insertPlainText("""
        [Trace Headers]""")
        help_text.setTextColor(Black)
        help_text.insertPlainText("""
        General:
            >> specify/load directories for building trace db
            >> select trace for header display
            >> select level of verbosity for display

        Buttons:""")
        help_text.setTextColor(Red)
        help_text.insertPlainText("""
            <Build Trace db>""")
        help_text.setTextColor(Black)
        help_text.insertPlainText(""": Searchs the directories listed in the "Data-
\tDirectories" entry box (a colon separated list) and builds a list of mseed
\tfiles found indexing them on unique values of <dir>:<file_name>. It then
\tcreates a dropdown menu with entries for each data directory found.""")  # noqa: E501
        help_text.setTextColor(Red)
        help_text.insertPlainText("""
            <Find>""")
        help_text.setTextColor(Black)
        help_text.insertPlainText(""": Launches a file browser allowing the user to
\tadd directories to the "Data Directories" entry box. Double clicking selects
\tthe new directory.""")  # noqa: E501
        help_text.setTextColor(Red)
        help_text.insertPlainText("""
            <Clear>""")
        help_text.setTextColor(Black)
        help_text.insertPlainText(""": Clears the \"Data Directories\"""" +
                                  """ entry box.\n""")
        help_text.insertPlainText("""
                By selecting a data directory a dropdown menu is created for
                trace selection. Selecting a trace from this menu will create
                a header display.

        Radio Buttons:""")
        help_text.setTextColor(Red)
        help_text.insertPlainText("""
            <Standard>""")
        help_text.setTextColor(Black)
        help_text.insertPlainText(""": This is the default. It displays Station, Channel, Location Code,
\tNet Code, and the nominal Sample Rate.""")  # noqa: E501
        help_text.setTextColor(Red)
        help_text.insertPlainText("""
            <Verbose>""")
        help_text.setTextColor(Black)
        help_text.insertPlainText(""": Standard display plus time of block.""")
        help_text.setTextColor(Red)
        help_text.insertPlainText("""
            <Very Verbose>""")
        help_text.setTextColor(Black)
        help_text.insertPlainText(""": Verbose plus the rest of the mseed fixed header fields.""")  # noqa: E501
        help_text.setTextColor(Red)
        help_text.insertPlainText(("""
            <Unique>"""))
        help_text.setTextColor(Black)
        help_text.insertPlainText(""": This displays changes in the """ +
                                  """Standard fields within a mseed file.""")
        help_text.insertPlainText("""

        Slider Scale and """)
        help_text.setTextColor(Red)
        help_text.insertPlainText("<Jump To Block #>")
        help_text.setTextColor(Black)
        help_text.insertPlainText(""" Button:
                At the bottom of the page is a scale bar showing block numbers. Sliding
                the bar scans through all fixed headers for the selected mseed file.\n""")  # noqa: E501
        help_text.setTextColor(Red)

        help_text.insertPlainText("""
        <Flush Dictionaries>""")
        help_text.setTextColor(Black)
        help_text.insertPlainText(
            """: Clears cached trace header data. As you view mseed header""" +
            """s,\n\tmseedpeek maintains a cache of previously viewed""" +
            """ headers for\n\tquick, future access. If you wish to """ +
            """ monitor changes to mseed files you\n\tneed to flush the """ +
            """dictionaries so that the new changes will be displayed.""")
        help_text.setTextColor(Green)
        help_text.insertPlainText("""

        [Blockettes]""")
        help_text.setTextColor(Black)
        help_text.insertPlainText(""":
                The 'Blockettes' notebook displays all blockettes found for the select
                mseed file in a dropdown menu. By selecting a blockette from the dropdown
                menu, the contents of the blockette will be displayed.""")  # noqa: E501
        help_text.setTextColor(Red)
        help_text.insertPlainText("""
            <Blockette Info>""")
        help_text.setTextColor(Black)
        help_text.insertPlainText(""": Displays a pop-up window with a definition of the SEED Blockette.

                At the bottom of the page is a scale bar showing block numbers and
                Sliding the bar scans through all blockettes for the selected mseed file.\n""")  # noqa: E501
        help_text.setTextColor(Blue)
        help_text.insertPlainText("\nKEYWORDS")
        help_text.setTextColor(Black)
        help_text.insertPlainText("""
        mseed; header information\n""")
        help_text.setTextColor(Blue)
        help_text.insertPlainText("\nSEE ALSO")
        help_text.setTextColor(Black)
        help_text.insertPlainText("""
        SEED manual (pdf), fixhdr, mseedhdr\n""")
        help_text.setTextColor(Blue)
        help_text.insertPlainText("\nAUTHOR")
        help_text.setTextColor(Black)
        help_text.insertPlainText("""
        Bruce Beaudoin <bruce@passcal.nmt.edu>
        """)

    def build_blockettes(self):
        """
        Create initial widgets for Blockettes tab
        """
        # data boxes
        self.blktype_box = QGroupBox()
        self.blktype_box.setObjectName("Box1")

        self.blkinfo_box = QGroupBox()
        self.blkinfo_box.setLayout(QHBoxLayout())
        self.blkinfo_box.setObjectName("Box2")

        self.blk_vars_box = QGroupBox()
        self.blk_vars_box.setLayout(QVBoxLayout())
        self.blk_vars_box.setObjectName("Box3")

        # widgets
        blk_label = QLabel("Blockette:")
        blk_menu = QComboBox()
        blk_menu.currentIndexChanged.connect(
            lambda: self.fill_blockettes_tab(
                0,
                blk_menu.currentText()))
        blk_menu.setObjectName("type_menu")

        self.blkinfo_btn = QPushButton("Blockette Info")
        self.blkinfo_btn.setStyleSheet("QPushButton{background-color:lightblue;}\
                                  QPushButton::hover{background-color:green;}")
        self.blkinfo_btn.clicked.connect(self.blkinfo_window)

        # box layouts
        l1 = QHBoxLayout(self.blktype_box)
        l1.addWidget(blk_label)
        l1.addWidget(blk_menu)
        l1.addStretch()

        # add to tab's main layout
        main_layout = QVBoxLayout()
        main_layout.addWidget(self.blktype_box)
        main_layout.addWidget(self.blkinfo_box)
        main_layout.addWidget(self.blk_vars_box)
        main_layout.addStretch()
        self.blockettes_tab.setLayout(main_layout)

    def build_trace_headers(self):
        """
        Build widgets for Trace Headers tab
        """
        # tab layout
        self.trace_headers_layout = QVBoxLayout(
            self.trace_headers_tab)

        # groupboxes for data fields
        self.datadir_box = QGroupBox()
        self.datadir_box.setCheckable(False)
        sp = self.datadir_box.sizePolicy()
        sp.setVerticalPolicy(QSizePolicy.Fixed)
        self.datadir_box.setSizePolicy(sp)

        self.stations_box = QGroupBox()
        self.stations_box.setCheckable(False)
        sp = self.stations_box.sizePolicy()
        sp.setVerticalPolicy(QSizePolicy.Fixed)
        self.stations_box.setSizePolicy(sp)

        self.radio_box = QGroupBox()
        self.radio_box.setCheckable(False)
        sp = self.radio_box.sizePolicy()
        sp.setVerticalPolicy(QSizePolicy.Fixed)
        self.radio_box.setSizePolicy(sp)

        self.dir_trace_box = QGroupBox()
        self.dir_trace_box.setCheckable(False)
        sp = self.dir_trace_box.sizePolicy()
        sp.setVerticalPolicy(QSizePolicy.Fixed)
        self.dir_trace_box.setSizePolicy(sp)

        self.flush_exit_box = QGroupBox()
        self.flush_exit_box.setCheckable(False)
        sp = self.flush_exit_box.sizePolicy()
        sp.setVerticalPolicy(QSizePolicy.Fixed)
        self.flush_exit_box.setSizePolicy(sp)
        self.window_layout.addWidget(self.flush_exit_box)

        # layouts for groupboxes
        datadir_layout = QHBoxLayout(self.datadir_box)
        stations_layout = QHBoxLayout(self.stations_box)
        rbuttons_layout = QHBoxLayout(self.radio_box)
        flush_exit_layout = QHBoxLayout(self.flush_exit_box)
        flush_exit_layout.setContentsMargins(0, 0, 0, 0)
        flush_exit_layout.setSpacing(450)
        dir_trace_layout = QFormLayout(self.dir_trace_box)

        # fill flush / exit groubpx
        self.exit_btn = QPushButton("Exit")
        self.exit_btn.setStyleSheet(
            "QPushButton::hover{background-color:darkred;}")
        self.exit_btn.clicked.connect(lambda: quit())
        self.flush_btn = QPushButton("Flush Directories")
        self.flush_btn.clicked.connect(self.flush_dict)
        self.flush_btn.setStyleSheet(
            "QPushButton::hover{background-color:orange;}")
        flush_exit_layout.addWidget(self.flush_btn)
        flush_exit_layout.addWidget(self.exit_btn)

        # fill data dir groupbox
        self.dd_label = QLabel("Data Directories:")
        self.dd_text = QLineEdit()
        self.build_trace_btn = QPushButton("Build Trace db")
        self.build_trace_btn.setStyleSheet(
            """
            QPushButton{background-color:lightblue;}
            QPushButton::hover {background-color:green;}
            """)
        self.build_trace_btn.clicked.connect(self.clicked_build_trace)
        self.find_btn = QPushButton("Find")
        self.find_btn.clicked.connect(self.clicked_find)
        self.clear_btn = QPushButton("Clear")
        self.clear_btn.clicked.connect(lambda: self.dd_text.clear())
        datadir_layout.addWidget(self.dd_label)
        datadir_layout.addWidget(self.dd_text)
        datadir_layout.addWidget(self.build_trace_btn)
        datadir_layout.addWidget(self.find_btn)
        datadir_layout.addWidget(self.clear_btn)

        # stations widgets
        self.stations_label = QLabel(
            "Find only stations (colon separated list):")
        self.stations_text = QLineEdit()
        self.stations_clear_btn = QPushButton("Clear")
        self.stations_clear_btn.clicked.connect(
            lambda: self.stations_text.clear())
        stations_layout.addWidget(self.stations_label)
        stations_layout.addWidget(self.stations_text)
        stations_layout.addWidget(self.stations_clear_btn)

        # fill stations groupbox
        self.standard_rbtn = QRadioButton("Standard")
        self.standard_rbtn.setChecked(True)
        self.standard_rbtn.clicked.connect(lambda: self.clicked_scan_type(0))
        self.verbose_rbtn = QRadioButton("Verbose")
        self.verbose_rbtn.setChecked(False)
        self.verbose_rbtn.clicked.connect(lambda: self.clicked_scan_type(1))
        self.vv_rbtn = QRadioButton("Very Verbose")
        self.vv_rbtn.setChecked(False)
        self.vv_rbtn.clicked.connect(lambda: self.clicked_scan_type(2))
        self.unique_rbtn = QRadioButton("Unique")
        self.unique_rbtn.setChecked(False)
        self.unique_rbtn.clicked.connect(lambda: self.clicked_scan_type(3))

        # spacer item
        # adds distance between rbuttons and "Header Endianess"
        spacerItem = QSpacerItem(250, 0,
                                 QSizePolicy.Expanding,
                                 QSizePolicy.Minimum)

        self.header_endianess_label = QLabel("Header Endianess")
        self.header_endianess_text = QLineEdit()
        self.header_endianess_text.setStyleSheet("background-color:yellow")

        rbuttons_layout.addWidget(self.standard_rbtn)
        rbuttons_layout.addWidget(self.verbose_rbtn)
        rbuttons_layout.addWidget(self.vv_rbtn)
        rbuttons_layout.addWidget(self.unique_rbtn)
        rbuttons_layout.addItem(spacerItem)
        rbuttons_layout.addWidget(self.header_endianess_label)
        rbuttons_layout.addWidget(self.header_endianess_text)

        # fill dir / trace groupbox
        self.dir_label = QLabel("Directory:")
        self.dir_menu = QComboBox()
        self.dir_menu.activated.connect(lambda: self.select_dir_trace("dir"))
        self.trace_label = QLabel("Trace:")
        self.trace_menu = QComboBox()
        self.trace_menu.activated.connect(
            lambda: self.select_dir_trace("trace"))
        dir_trace_layout.addRow(self.dir_label, self.dir_menu)
        dir_trace_layout.addRow(self.trace_label, self.trace_menu)
        self.dir_trace_box.hide()
        self.trace_label.hide()
        self.trace_menu.hide()

        # stacked widget (jump box)
        self.standard_box = StandardBox(self.standard_vars)
        self.verbose_box = VerboseBox(self.standard_vars, self.v_vars)
        self.vv_box = VVBox(
            self.first_vv_vars, self.standard_vars, self.v_vars, self.vv_vars)
        self.unique_box = UniqueBox()
        self.unique_box.unique_jump.currentIndexChanged.connect(
            lambda: self.update_slider("unique"))
        self.unique_box.keys_menu.currentIndexChanged.connect(
            self.select_keys)

        self.jump_box = QStackedWidget()
        self.jump_box.setStyleSheet("QGroupBox{border:0;}")
        self.jump_box.setMinimumSize(0, 200)
        self.jump_box.addWidget(self.standard_box)
        self.jump_box.addWidget(self.verbose_box)
        self.jump_box.addWidget(self.vv_box)
        self.jump_box.addWidget(self.unique_box)

        # add everything to trace headers layout
        self.trace_headers_layout.addWidget(self.datadir_box)
        self.trace_headers_layout.addWidget(self.stations_box)
        self.trace_headers_layout.addWidget(self.radio_box)
        self.trace_headers_layout.addWidget(self.dir_trace_box)
        self.trace_headers_layout.addWidget(self.jump_box)

        # set data directory widget to display cwd
        default_dir = os.getcwd()
        self.dd_text.setText(default_dir)

    def clicked_scan_type(self, index):
        """
        Show data related to selected scan type
        """
        self.verb_var = index
        self.jump_box.setCurrentIndex(self.verb_var)
        if self.trace_menu.isHidden():
            self.jump_box.hide()
        else:
            self.jump_box.show()
            if self.verb_var == 3:
                self.slider_box.hide()
            else:
                self.slider_box.show()
        self.maybe_read_hdrs()

    def clicked_find(self):
        """
        Open file dialogue to search for mseed files
        """
        search = QFileDialog.getExistingDirectory()
        directories = self.dd_text.text()

        if search:
            if directories:
                self.dd_text.insert(":" + search)
            else:
                self.dd_text.insert(search)

    def clicked_build_trace(self):
        """
        Check directories have been provided before building trace list
        """
        directories = self.dd_text.text()
        if directories:
            self.dir_list.clear()
            self.hide_show("hide")
            self.build_trace_list(directories)
        else:
            error_msg = QMessageBox()
            error_msg.setIcon(QMessageBox.Critical)
            error_msg.setText("Error")
            error_msg.setInformativeText("No directories listed.")
            error_msg.setWindowTitle("Error")
            error_msg.exec_()

    def build_trace_list(self, directories):
        """
        Build trace list from given directories
        """
        for dir in str(directories).split(":"):
            if not os.path.isdir(dir):
                err = "***WARNING*** Directory " + dir + " not found."
                self.infobar(err, "red")
            else:
                self.dir_list.append(dir)

        stat_sel = self.stations_text.text()
        if stat_sel:
            for stat in str(stat_sel).split(":"):
                statsel = stat.strip()
                self.stat_sel_list.append(statsel)

        if self.dir_list:
            self.launch_find_trace()

    def hide_show(self, action):
        """
        Hide/show widgets between scans
        """
        if action == "hide":
            widget = self.jump_box.currentWidget()
            for child in widget.findChildren(QWidget):
                if not child.isHidden():
                    child.hide()
            for child in self.slider_box.findChildren(QWidget):
                if not child.isHidden():
                    child.hide()
            widgets = self.blockettes_tab.findChildren(QGroupBox)
            for widget in widgets:
                if not widget.isHidden():
                    widget.hide()
            self.trace_label.hide()
            self.trace_menu.hide()
            self.blktype_box.hide()
            self.blkinfo_box.hide()
            self.blk_vars_box.hide()
        else:
            widget = self.jump_box.currentWidget()
            for child in widget.findChildren(QWidget):
                if child.isHidden():
                    child.show()
            for child in self.slider_box.findChildren(QWidget):
                if child.isHidden():
                    child.show()
            widgets = self.blockettes_tab.findChildren(QGroupBox)
            for widget in widgets:
                if widget.isHidden():
                    widget.show()
            self.blktype_box.show()
            self.blkinfo_box.show()
            self.blk_vars_box.show()

    def launch_find_trace(self):
        """
        Separated out to run build trace list
        as a thread
        """

        self.run_find_trace = 1
        self.begin_thread("Find Trace",
                          self.find_trace,
                          self.after_find_trace)

    def find_trace(self):
        """
        based on traverse routine in "python standard library", Lundh pg 34
        """

        stack = []
        for k in range(len(self.dir_list)):
            stack.append(self.dir_list[k])

        file_list = {}
        num_mseed_files = 0
        rw_error = 0
        cnt = 1
        while stack:
            directory = stack.pop()
            try:
                listfiles = os.listdir(directory)
            except Exception as e:
                print("Directory Read Error: %s" % e)

            for file in listfiles:
                if not self.run_find_trace:
                    break
                if mod(cnt, 5):
                    pass
                else:
                    self.func_worker.update.emit(cnt)
                    # self.wait("Examining File: ", cnt)
                fullname = os.path.join(directory, file)
                if os.path.isfile(fullname):
                    if not os.access(fullname, 6):
                        err = "ERROR: Read/Write permission denied."
                        err1 = "\t File: " + fullname
                        print(err)
                        print(err1)
                        rw_error += 1
                        continue
                    try:
                        msfile = Mseed(fullname)
                        if msfile.isMseed():
                            num_mseed_files += 1
                            if directory in file_list:
                                file_list[directory].append(file)
                            else:
                                file_list[directory] = []
                                file_list[directory].append(file)
                            try:
                                # simple test to determine if correct size file
                                filesize = msfile.filesize
                                blksize = msfile.blksize
                                (numblocks, odd_size) = divmod(
                                    filesize, blksize)
                                if odd_size:
                                    warn = ("ERROR: File size is not an "
                                            " integer number of block size (" +
                                            str(blksize) + ").")
                                    warn += " Header reading may not work."
                                    warn += " To fix variable record length"
                                    warn += ", use fixhdr."
                                    warn += f"\n\t File: {fullname}"
                                    print(warn)
                                    rw_error += 1
                                    continue
                            except Exception:
                                err = ("ERROR: Cannot determine file and "
                                       "block sizes. \n\t File:" + fullname)
                                print(err)
                                rw_error += 1
                                continue
                        msfile.close()
                    except Exception as e:
                        rw_error += 1
                        print("File Open Error: %s" % fullname, e)
                cnt += 1
                if (os.path.isdir(fullname) or (os.path.islink(fullname) and
                                                not os.path.isfile(fullname))):
                    stack.append(fullname)

        self.dir_trace_dict = {}
        self.dir_trace_dict = file_list
        self.num_files = num_mseed_files
        self.err_files = rw_error

    def after_find_trace(self):
        """
        Function to run after find trace
        to update some info
        """

        self.update_dir_list()
        text = ("Done. " + str(self.num_files) + " mseed files found. ***")
        text += (str(self.err_files) + " files with errors.")
        self.update_infobar(text, "green")

    def select_dir_trace(self, menu):
        """
        If directory menu selected, show trace menu
        If trace menu selected, read hdrs
        """
        if menu == "dir":
            dir = self.dir_menu.currentText()
            if dir:
                self.update_trace_list(dir)
                self.trace_label.show()
                self.trace_menu.show()
            else:
                self.trace_label.hide()
                self.trace_menu.hide()
        else:
            trace = self.trace_menu.currentText()
            if trace:
                dir = self.dir_menu.currentText()
                self.next_scan(dir, trace)

    def next_scan(self, dir, trace):
        """
        Prepare for next scan, i.e. reset/clear/hide some widgets
        """
        self.clear_slider()
        self.read_hdrs(dir, trace, 0)
        self.fill_slider()
        self.hide_show("show")
        self.header_endianess_text.setText(self.byte_order)
        self.jump_box.setCurrentIndex(self.verb_var)
        self.jump_box.show()
        self.update_infobar("", "yellow")

        # hack, widget was behaving weird for some reason
        # after showing it
        self.slider_box.jump_menu.setEnabled(False)
        self.slider_box.jump_menu.setEnabled(True)

    def read_hdrs(self, dir, trace, blk):
        """
        Read headers of a given file, build dictionary for quick access
        """
        file_absolute_path = os.path.join(dir, trace)

        rdfile = Mseed(file_absolute_path)
        if rdfile.isMseed():
            try:
                filesize = rdfile.filesize
                blksize = rdfile.blksize
            except Exception:
                err = "Cannot determine file and block sizes. File: " + trace
                self.update_infobar(err, "red")
                rdfile.close()
                return

            self.blk_size = blksize
            (numblocks, self.odd_size) = divmod(filesize, blksize)
            self.num_blocks = numblocks
            verbose = self.verb_var

            self.fixed_hdr_dict = {}
            self.rate_dict = {}
            self.blockettes_dict = {}
            self.key_list = []
            self.unique_list = []
            self.unique_select_list = []
            n = 0

            # now build a dictionary of all fixed header info keyed to block
            # number looping over total number of blocks in files
            lastkey = -1
            while n < numblocks:
                hdrs = rdfile.fixedhdr(n * blksize)
                self.fixed_hdr_dict[n] = hdrs
                self.rate_dict[n] = rdfile.rate

                # if unique selected build unique list
                (SeqNum, DHQual, res, Stat, Loc, Chan, Net) = hdrs[0]
                Rate = str(self.rate_dict[n])
                Stat = Stat.strip().decode()
                Chan = Chan.strip().decode()
                Loc = Loc.strip().decode()
                Net = Net.strip().decode()
                key = ":".join([Stat, Chan, Loc, Net, Rate])
                # if key in self.keyList:
                if key == lastkey:
                    pass
                else:
                    lastkey = key
                    if key not in self.unique_select_list:
                        self.unique_select_list.append(key)
                    key = str(n) + ":" + key
                    self.unique_list.append(key)

                # build Blockette dictionary keyed to block number
                numblk = hdrs[3][3]
                addseek = hdrs[3][6]
                b = 0

                # loop over number of blockettes following fixed header at
                # block n
                while b < numblk:
                    seekval = (n * blksize) + addseek
                    (blktype, next) = rdfile.typenxt(seekval)
                    # builds command to read specific blockette
                    blklist = eval("rdfile.blk" + str(blktype) + "\
                                   (" + str(seekval) + ")")
                    if blklist:
                        # handle awkward lists w/ muli-byte reserve, array, or
                        # calib fields
                        if blklist[0] == 100:
                            tmplist = blklist[:4]
                            tup = blklist[4:]
                            tmplist.append(tup)
                            blklist = tmplist
                        if blklist[0] == 201:
                            tmplist = blklist[:8]
                            tup = blklist[8:13]
                            for new in (tup, blklist[14], blklist[15],
                                        blklist[16]):
                                tmplist.append(new)
                            blklist = tmplist
                        if blklist[0] == 400:
                            tmplist = blklist[:5]
                            tup = blklist[5:]
                            tmplist.append(tup)
                            blklist = tmplist
                        # build Blockettes
                        if n in self.blockettes_dict:
                            self.blockettes_dict[n].append(blklist)
                        else:
                            self.blockettes_dict[n] = []
                            self.blockettes_dict[n].append(blklist)
                    addseek = next
                    b += 1
                n += 1
            # get endianess before closing
            self.byte_order = rdfile.byteorder
            rdfile.close()

            self.set_blockettes(0)
            if not verbose:
                self.fill_standard(int(blk))
            elif verbose == 1:
                self.fill_verbose(int(blk))
            elif verbose == 2:
                self.fill_vv(int(blk))
            else:
                self.fill_unique()

    def clear_slider(self):
        """
        Reset blk slider between scans
        """
        # block/unblock widget signals
        self.slider_box.slider.blockSignals(True)
        self.slider_box.jump_menu.blockSignals(True)
        self.slider_box.slider.setValue(0)
        self.slider_box.jump_menu.clear()
        self.slider_box.jump_menu.insertItem(0, "0")
        self.slider_box.jump_menu.setCurrentIndex(0)
        self.slider_box.jump_menu.blockSignals(False)
        self.slider_box.slider.blockSignals(False)

    def fill_slider(self):
        """
        Set slider range for chosen trace
        """
        # block/unblock widget signals
        self.slider_box.slider.blockSignals(True)
        self.slider_box.jump_menu.blockSignals(True)
        self.slider_box.slider.setTickPosition(
            QSlider.TicksBelow)
        self.slider_box.slider.setRange(0, self.num_blocks - 1)
        i = 0
        self.slider_box.jump_menu.clear()
        while i < self.num_blocks:
            self.slider_box.jump_menu.insertItem(i, str(i))
            i += 1
        self.slider_box.slider.setTickInterval(
            self.slider_box.slider.maximum() / 5)
        self.slider_box.jump_menu.blockSignals(False)
        self.slider_box.slider.blockSignals(False)

    def set_blockettes(self, key):
        """
        Setup blockette frames for new key entry
        """
        self.blockettes_list = []
        for blk in self.blockettes_dict[key]:
            self.blockettes_list.append(blk[0])
        self.blockettes_list.sort()

        # fill blockette info for first blockette found
        self.fill_blockettes_tab(key, self.blockettes_list[0])

    def fill_blockettes_tab(self, key, blktype):
        """
        clears/initializes entry fields in Blockettes tab
        """

        for block in self.blockettes_dict[key]:
            if int(block[0]) == int(blktype):
                blocktuple = block
        for key, values in BlkVars.items():
            if int(blktype) == int(key):
                boxes = self.blockettes_tab.findChildren(QGroupBox)
                for box in boxes:
                    if box.objectName() == "Box1":
                        menu = box.findChild(QComboBox, "type_menu")
                        menu.blockSignals(True)
                        menu.clear()
                        for blk in self.blockettes_list:
                            menu.addItem(str(blk))
                        menu.setCurrentText(str(blktype))
                        menu.blockSignals(False)
                    elif box.objectName() == "Box2":
                        layout = box.layout()
                        # delete previous labels if they exist
                        if layout is not None:
                            while layout.count():
                                item = layout.takeAt(0)
                                widget = item.widget()
                                if widget is not None:
                                    widget.setParent(None)
                        # create new labels
                        label = QLabel()
                        label.setText(str(values[0]))
                        layout.addWidget(label)
                        layout.addWidget(self.blkinfo_btn)
                        layout.addStretch()
                    else:
                        layout = box.layout()
                        # delete previous labels if they exist
                        if layout is not None:
                            while layout.count():
                                item = layout.takeAt(0)
                                widget = item.widget()
                                if widget is not None:
                                    widget.setParent(None)
                        # create new labels
                        for x in range(1, len(values)):
                            label = QLabel()
                            label.setText(str(values[x]) + " " +
                                          str(blocktuple[x - 1]))
                            layout.addWidget(label)

    def blkinfo_window(self):
        """
        Popup window for blk info button
        """
        self.info_window = QWidget()
        self.info_window.resize(650, 400)
        self.info_window.setWindowTitle("Blockette Information")

        # widgets
        blk_label = QLabel("Blockette:")
        blk_menu = QComboBox()
        blk_menu.activated.connect(
            lambda: info_box.setText(BlkInfoDict[int(blk_menu.currentText())])
            if blk_menu.currentText().isdigit()
            else info_box.setText(BlkInfoDict[blk_menu.currentText()]))
        info_box = QTextEdit()
        done_btn = QPushButton("Done")
        done_btn.setStyleSheet("QPushButton{background-color:lightblue;}\
                                QPushButton::hover{background-color:red;}")
        done_btn.clicked.connect(lambda: self.info_window.close())

        top_layout = QHBoxLayout()
        top_layout.addWidget(blk_label)
        top_layout.addWidget(blk_menu)
        top_layout.addStretch()

        main_layout = QVBoxLayout(self.info_window)
        main_layout.addLayout(top_layout)
        main_layout.addWidget(info_box)
        main_layout.addWidget(done_btn)

        for key in BlkInfoDict.keys():
            blk_menu.addItem(str(key))
        blk_menu.setCurrentText(str(self.blockettes_list[0]))

        info_box.setText(BlkInfoDict[self.blockettes_list[0]])

        self.info_window.show()

    def fill_standard(self, key):
        """
        Fills standard info on HdrDisplay
        """
        try:
            standard_widgets = self.standard_box.findChildren(
                QLineEdit)
            rate = self.rate_dict[key]
            vars = []
            # SeqNum, DHQual, res, Stat, Loc, Chan, Net
            vars.append(self.fixed_hdr_dict[key][0])
            vars = list(itertools.chain.from_iterable(vars))
            vars = vars[3:]  # remove SeqNum, DHQual, res
            vars.append(rate)

            for x in range(len(vars)):
                widget = standard_widgets[x]
                var = vars[x]
                try:
                    var = var.strip().decode()
                except Exception:
                    pass
                widget.setText(str(var))

        # if user inputs invalid blk num
        except Exception:
            return

    def fill_verbose(self, key):
        """
        Fills verbose info on HdrDisplay
        """
        try:
            verbose_widgets = self.verbose_box.findChildren(
                QLineEdit)
            rate = self.rate_dict[key]
            vars = []
            # SeqNum, DHQual, res, Stat, Loc, Chan, Net
            vars.append(self.fixed_hdr_dict[key][0])
            vars.append([str(rate)])
            # Year, Day, Hour, Min, Sec, junk, Micro
            vars.append(self.fixed_hdr_dict[key][1])
            vars = list(itertools.chain.from_iterable(vars))
            vars = vars[3:]  # remove SeqNum, DHQual, res
            del vars[-2]  # remove junk

            for x in range(len(vars)):
                widget = verbose_widgets[x]
                var = vars[x]
                try:
                    var = var.strip().decode()
                except Exception:
                    pass
                widget.setText(str(var))

        # if user inputs invalid blk num
        except Exception:
            return

    def fill_vv(self, key):
        """
        Fills very verbose info on HdrDisplay
        """
        try:
            vv_widgets = self.vv_box.findChildren(QLineEdit)
            rate = self.rate_dict[key]
            vars = []
            # SeqNum, DHQual, res, Stat, Loc, Chan, Net
            vars.append(self.fixed_hdr_dict[key][0])
            vars.append([str(rate)])
            # Year, Day, Hour, Min, Sec, junk, Micro
            vars.append(self.fixed_hdr_dict[key][1])
            # NumSamp, SampFact, SampMult
            vars.append(self.fixed_hdr_dict[key][2])
            # act, io, DQual, numblk, timcorr, bdata, bblock
            vars.append(self.fixed_hdr_dict[key][3])
            vars = list(itertools.chain.from_iterable(vars))
            del vars[-12]  # remove junk

            for x in range(len(vars)):
                widget = vv_widgets[x]
                var = vars[x]
                try:
                    var = var.strip().decode()
                except Exception:
                    pass
                widget.setText(str(var))

        # if user inputs invalid blk num
        except Exception:
            return

    # def update_unique_block(self):
    #     """
    #     Update selected item in Unique's tree widget
    #     """

    #     row_num = int(self.unique_box.unique_jump.currentText())
    #     items = self.unique_box.unique_info_tree.findItems(
    #         str(row_num), Qt.MatchExactly, 0)
    #     self.unique_box.unique_info_tree.clearSelection()
    #     for i in items:
    #         self.unique_box.unique_info_tree.setItemSelected(i, True)
    #         self.unique_box.unique_info_tree.scrollToItem(i)

    def select_keys(self):
        """
        Display blocks from selected key
        """

        selectkey = self.unique_box.keys_menu.currentText().split(":")
        if selectkey[0] == "*":
            self.fill_unique()
            return
        self.unique_box.unique_info_tree.clear()
        for key in self.unique_list:
            if selectkey == key.split(":")[1:]:
                unique_item = QTreeWidgetItem()
                line = []
                for var in key.split(":"):
                    line.append(var)
                for i in range(len(line)):
                    unique_item.setText(i, line[i])
                    unique_item.setBackground(i, QBrush(Qt.white))
                self.unique_box.unique_info_tree.addTopLevelItem(unique_item)

    def fill_unique(self):
        """
        Fills unique info on HdrDisplay
        """

        # fill drop down menus
        self.unique_box.keys_menu.blockSignals(True)
        self.unique_box.keys_menu.clear()
        self.unique_box.keys_menu.addItem("*")
        for key in self.unique_select_list:
            self.unique_box.keys_menu.addItem(key)
        self.unique_box.keys_menu.blockSignals(False)

        self.unique_box.unique_jump.blockSignals(True)
        i = 0
        while i < self.num_blocks:
            self.unique_box.unique_jump.addItem(str(i))
            i += 1
        self.unique_box.unique_jump.blockSignals(False)

        # fill tree
        self.unique_box.unique_info_tree.clear()
        c = 0
        for key in self.unique_list:
            unique_item = QTreeWidgetItem()
            line = []
            for var in key.split(":"):
                line.append(var)
            if c:
                for i in range(len(line)):
                    unique_item.setText(i, line[i])
                    unique_item.setBackground(i, QBrush(Qt.cyan))
            else:
                for i in range(len(line)):
                    unique_item.setText(i, line[i])
            self.unique_box.unique_info_tree.addTopLevelItem(unique_item)
            if c:
                c = 0
            else:
                c += 1

    def wait(self, words, cnt):
        """
        routine to put words on info bar, used to count traces
        being examined
        """
        text = words + str(cnt)
        self.update_infobar(text, "lightblue")

    def update_dir_list(self):
        """
        Update directory list for each scan
        """
        self.dir_menu.clear()
        self.dir_menu.insertItem(0, "")
        for key in self.dir_trace_dict:
            self.dir_menu.addItem(key)
        self.dir_trace_box.show()

    def update_trace_list(self, dir):
        """
        Update trace list for each scan
        """
        self.trace_menu.clear()
        self.trace_menu.insertItem(0, "")
        for key, values in self.dir_trace_dict.items():
            if dir == key:
                values.sort()
                for trace in values:
                    self.trace_menu.addItem(trace)

    def update_infobar(self, text, color):
        """
        Update info bar when certain actions happen
        """
        self.infobar.setText(text)
        self.infobar.setStyleSheet("background-color:" + color)
        if color != "yellow":
            QApplication.beep()

    def update_slider(self, widget):
        """
        Get new blk number if any of the slider related widgets
        are toggled, and update hdr info
        """
        try:
            if widget == "slider":
                value = self.slider_box.slider.value()
                self.slider_box.jump_menu.setCurrentText(str(value))
                self.unique_box.unique_jump.setCurrentText(str(value))
            elif widget == "menu":
                value = self.slider_box.jump_menu.currentText()
                self.slider_box.slider.setValue(int(value))
                self.unique_box.unique_jump.setCurrentText(str(value))
            else:
                value = self.unique_box.unique_jump.currentText()
                self.slider_box.slider.setValue(int(value))
                self.slider_box.jump_menu.setCurrentText(str(value))
                self.vv_rbtn.setChecked(True)
                self.verb_var = 2
                self.jump_box.setCurrentIndex(2)

            # update hdr info & info bar
            self.maybe_read_hdrs()
            byte_offset = int(value) * int(self.blk_size)
            text = "Byte Offset: " + str(byte_offset)
            self.update_infobar(text, "yellow")

        # if user inputs invalid blk num
        except Exception:
            return

    def maybe_read_hdrs(self):
        """
        If certain conditions are met, call read hdrs
        """
        if not self.dir_menu.isHidden():
            dir = self.dir_menu.currentText()
            if dir:
                if not self.trace_menu.isHidden():
                    trace = self.trace_menu.currentText()
                    if trace:
                        blk = self.slider_box.jump_menu.currentText()
                        self.read_hdrs(dir, trace, blk)

    def flush_dict(self):
        """
        clears dictionaries. Allows user to apply a reset and
        dynamically monitor changes to mseed files.
        """
        self.fixed_hdr_dict = {}
        self.rate_dict = {}
        self.blockettes_dict = {}
        self.key_list = []
        self.unique_list = []
        self.clear_slider()
        self.header_endianess_text.clear()
        self.hide_show("hide")
        QApplication.beep()

    def begin_thread(self, title, func, after_func=''):
        """
        Create thread to run function
        """

        # init thread and worker
        self.thread = QThread(self)
        self.func_worker = Worker(func)

        # set signals/slots
        self.func_worker.update.connect(self.update_progress_window)
        self.thread.started.connect(self.func_worker.run)

        # function to run after thread finishes
        if after_func:
            self.func_worker.finished.connect(after_func)

        # delete worker and thread when done
        self.func_worker.finished.connect(self.thread.quit)
        self.func_worker.finished.connect(self.func_worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)

        # get file num for progress window
        numfiles = 0
        text = ""

        if title == "Find Trace":
            for dir in self.dir_list:
                numfiles += (
                    sum([len(files) for r, d, files in os.walk(dir)]))
            text = title + " is active. Please wait."

        # launch progress window
        self.build_progress_window(title, text, numfiles)
        self.func_worker.finished.connect(
            lambda: self.progress_window.done(0))

        # give progress window a small
        # amount of time to finish
        # loading before
        # starting thread
        loop = QEventLoop(self)
        QTimer.singleShot(100, loop.quit)
        loop.exec_()

        self.thread.start()

    def build_progress_window(self, title, text, max):
        """
        Create progress window to update user
        on status of current process
        """

        self.progress_window = QProgressDialog(
            labelText=text, minimum=0,
            maximum=max, parent=self)

        cancel_b = QPushButton("Cancel")
        cancel_b.setStyleSheet(
            "QPushButton::hover{background-color:red;}")
        cancel_b.clicked.connect(
            lambda: self.stop_thread(title))
        self.progress_window.setCancelButton(cancel_b)
        self.progress_window.open()

    def update_progress_window(self, val):
        """
        Update progress window progress bar
        """

        self.progress_window.setValue(val)

    def stop_thread(self, title):
        """
        Stop the currently running thread
        """

        if title == "Find Trace":
            self.run_find_trace = 0


class SliderBox(QGroupBox):
    """
    Box that holds all the blockette slider widgets
    """
    def __init__(self):
        """
        Init widgets for blockette parsing
        """
        super().__init__()
        self.setStyleSheet("QGroupBox{border:0;}")
        # widgets
        label1 = QLabel("Jump To Block #:")
        self.jump_menu = QComboBox()
        self.jump_menu.setMinimumWidth(100)
        self.jump_menu.setEditable(True)
        self.jump_menu.insertItem(0, "0")
        label2 = QLabel("Block Number")
        self.slider = QSlider(Qt.Horizontal)
        self.slider.setTickPosition(self.slider.tickPosition().NoTicks)

        # layouts
        main_layout = QVBoxLayout(self)
        menu_layout = QVBoxLayout()
        slider_layout = QHBoxLayout()
        menu_layout.addWidget(label1, alignment=Qt.AlignCenter)
        menu_layout.addWidget(self.jump_menu, alignment=Qt.AlignCenter)
        slider_layout.addWidget(label2)
        slider_layout.addWidget(self.slider)

        # add to main layout
        main_layout.addLayout(menu_layout)
        main_layout.addLayout(slider_layout)


class StandardBox(QGroupBox):
    """
    Box that holds all the standard info widgets
    """
    def __init__(self, standard_vars):
        """
        Create standard widgets
        """
        super().__init__()

        # layout
        v_layout = QVBoxLayout(self)
        h_layout = QHBoxLayout()
        col1 = QVBoxLayout()
        col2 = QVBoxLayout()

        # widgets
        for x in range(len(standard_vars)):
            label = QLabel(str(standard_vars[x]))
            line_edit = QLineEdit()

            col1.addWidget(label)
            col2.addWidget(line_edit)

        # add to layout
        h_layout.addLayout(col1)
        h_layout.addLayout(col2)
        v_layout.addLayout(h_layout)

        v_layout.addStretch()


class VerboseBox(QGroupBox):
    """
    Box that holds all the verbose info widgets
    """
    def __init__(self, standard_vars, v_vars):
        """
        Create verbose widgets
        """
        super().__init__()
        # layout
        v_layout = QVBoxLayout(self)
        h_layout = QHBoxLayout()
        col1 = QVBoxLayout()
        col2 = QVBoxLayout()
        col3 = QVBoxLayout()
        col4 = QVBoxLayout()

        # widgets
        for x in range(len(standard_vars)):
            label = QLabel(str(standard_vars[x]))
            line_edit = QLineEdit()

            col1.addWidget(label)
            col2.addWidget(line_edit)

        for x in range(len(v_vars)):
            label = QLabel(str(v_vars[x]))
            line_edit = QLineEdit()

            if x == 0:
                col1.addWidget(label)
                col2.addWidget(line_edit)
            else:
                col3.addWidget(label)
                col4.addWidget(line_edit)

        col3.addSpacing(25)
        col4.addSpacing(25)

        # add to layout
        h_layout.addLayout(col1)
        h_layout.addLayout(col2)
        h_layout.addLayout(col3)
        h_layout.addLayout(col4)
        v_layout.addLayout(h_layout)
        v_layout.addStretch()


class VVBox(QGroupBox):
    """
    Box that holds all the very verbose info widgets
    """
    def __init__(self, first_vv_vars, standard_vars, v_vars, vv_vars):
        """
        Create very verbose widgets
        """
        super().__init__()
        # layout
        v_layout = QVBoxLayout(self)
        h_layout = QHBoxLayout()
        col1 = QVBoxLayout()
        col2 = QVBoxLayout()
        col3 = QVBoxLayout()
        col4 = QVBoxLayout()
        col5 = QVBoxLayout()
        col6 = QVBoxLayout()
        col7 = QVBoxLayout()
        col8 = QVBoxLayout()

        # widgets
        for x in range(len(first_vv_vars)):
            label = QLabel(str(first_vv_vars[x]))
            line_edit = QLineEdit()

            col1.addWidget(label)
            col2.addWidget(line_edit)

        for x in range(len(standard_vars)):
            label = QLabel(str(standard_vars[x]))
            line_edit = QLineEdit()

            if x <= 2:
                col1.addWidget(label)
                col2.addWidget(line_edit)
            else:
                col3.addWidget(label)
                col4.addWidget(line_edit)

        for x in range(len(v_vars)):
            label = QLabel(str(v_vars[x]))
            line_edit = QLineEdit()

            if x <= 3:
                col3.addWidget(label)
                col4.addWidget(line_edit)
            else:
                col5.addWidget(label)
                col6.addWidget(line_edit)

        for x in range(len(vv_vars)):
            label = QLabel(str(vv_vars[x]))
            line_edit = QLineEdit()

            if x <= 3:
                col5.addWidget(label)
                col6.addWidget(line_edit)
            elif x > 3 and x <= 6:
                col7.addWidget(label)
                col8.addWidget(line_edit)
            else:
                col7.addWidget(label)
                col8.addWidget(line_edit)

        # add to layout
        h_layout.addLayout(col1)
        h_layout.addLayout(col2)
        h_layout.addLayout(col3)
        h_layout.addLayout(col4)
        h_layout.addLayout(col5)
        h_layout.addLayout(col6)
        h_layout.addLayout(col7)
        h_layout.addLayout(col8)
        v_layout.addLayout(h_layout)
        v_layout.addStretch()


class UniqueBox(QGroupBox):
    """
    Box that holds all the unique info widgets
    """
    def __init__(self):
        """
        Create unique widgets
        """
        super().__init__()
        layout = QVBoxLayout(self)
        select_keys_label = QLabel("Select Keys:")
        self.keys_menu = QComboBox()
        # self.keys_menu.setEditable(True)
        jump_label = QLabel("Jump To Block #:")
        self.unique_jump = QComboBox()
        self.unique_jump.setEditable(True)
        self.unique_info_tree = QTreeWidget()
        self.unique_info_tree.setRootIsDecorated(False)
        self.unique_info_tree.setColumnCount(6)
        self.unique_info_tree.setHeaderLabels(
            ['Block', 'Stat', 'Chan', 'Loc', 'Net', 'Rate'])

        layout.addWidget(select_keys_label)
        layout.addWidget(self.keys_menu)
        layout.addWidget(jump_label)
        layout.addWidget(self.unique_jump)
        layout.addWidget(self.unique_info_tree)
        layout.addStretch()


class Worker(QObject):
    update = Signal(int)
    finished = Signal()

    def __init__(self, func):
        super().__init__()
        self.func = func

    def run(self):
        self.func()
        self.finished.emit()


if __name__ == "__main__":
    main()
