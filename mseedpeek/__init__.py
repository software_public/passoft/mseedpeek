# -*- coding: utf-8 -*-

"""Top-level package for mseedpeek."""

__author__ = """EPIC"""
__email__ = 'software-support@passcal.nmt.edu'
__version__ = '2024.4.0.1'
