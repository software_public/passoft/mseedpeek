#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst', 'rt') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst', 'rt') as history_file:
    history = history_file.read()


setup(
    author="IRIS PASSCAL",
    author_email='software-support@passcal.nmt.edu',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved ::  GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
    ],
    description="View mseed headers",
    entry_points={
        'console_scripts': [
            'mseedpeek=mseedpeek.mseedpeek:main',
        ],
    },
    install_requires=['PySide6'],
    setup_requires=[],
    extras_require={
        'dev': [
            'flake8',
            'tox',
        ]
    },
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='mseedpeek',
    name='mseedpeek',
    packages=find_packages(include=['mseedpeek']),
    url='https://git.passcal.nmt.edu/software_public/passoft/mseedpeek',
    version='2024.4.0.1',
    zip_safe=False,
)
