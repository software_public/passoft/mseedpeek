=========
mseedpeek
=========

* Description: View mseed headers

* Usage: mseeedpeek
         mseedpeek -#
         mseedpeek -f file(s)

* Free software: GNU General Public License v3 (GPLv3)
