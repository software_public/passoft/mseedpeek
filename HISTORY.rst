=======
History
=======

2018.129 (2018-06-07)
------------------

* First release on new build system.

2020.204 (2020-07-22)
------------------
* Updated to work with Python 3
* Added a unit test to test mseedpeek import
* Updated list of platform specific dependencies to be installed when pip
  installing mseedpeek (see setup.py)
* Installed and tested mseedpeek against Python3.[6,7,8] using tox
* Formatted Python code to conform to the PEP8 style guide
* Created conda packages for mseedpeek that can run on Python3.[6,7,8]
* Updated .gitlab-ci.yml to run a linter and unit tests for Python3.[6,7,8]
  in GitLab CI pipeline

2022.1.0.0 (2022-01-11)
------------------
* New versioning scheme

2022.2.0.0 (2023-04-03)
------------------
* Gui updated to PySide2
* Pmw dependency dropped
* Version number updated

2024.4.0.0 (2024-02-01)
------------------
* Gui updated to PySide6
