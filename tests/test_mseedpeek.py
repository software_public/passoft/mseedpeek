#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `mseedpeek` package."""

import unittest

from unittest.mock import patch
from mseedpeek.mseedpeek import main


class TestMseedpeek(unittest.TestCase):
    """Tests for `mseedpeek` package."""

    def test_import(self):
        """Test mseedpeek import"""
        with patch('sys.argv', ['mseedpeek', '-#']):
            with self.assertRaises(SystemExit) as cmd:
                main()
            self.assertEqual(cmd.exception.code, 0, "sys.exit(0) never called "
                             "- Failed to exercise mseedpeek")
